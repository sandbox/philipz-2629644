<?php

/**
 * @file
 * Contains \Drupal\video_embed_field\Plugin\migrate\cckfield\EmvideoField.
 */

namespace Drupal\video_embed_field\Plugin\migrate\cckfield;

use Drupal\migrate\Entity\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\cckfield\CckFieldPluginBase;

/**
 * @MigrateCckField(
 *   id = "emvideo"
 * )
 */
class EmvideoField extends CckFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldType(Row $row) {
    return 'video_embed_field';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'default' => 'video',
      'video' => 'video',
      'thumbnail' => 'thumbnail'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processCckFieldValues(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'iterator',
      'source' => $field_name,
      'process' => array(
        'value' => 'embed'
      )
    ];
    $migration->mergeProcessOfProperty($field_name, $process);
  }

}
